// This will use Deviare to hook every call in the AI.exe that is used
// for the VR support. These calls are all from the very old (0.3.2) Oculus SDK.
// All calls listed here came directly from the AI.exe.
//
// We'll hook all of these to log their usage, but it's unlikely we'll need
// them all when translating from ancient SDK to OpenVR.
//
// ovr_Initialize
// ovr_Shutdown
// ovr_GetTimeInSeconds
// ovr_WaitTillTime
// ovrHmd_BeginEyeRender
// ovrHmd_BeginFrame
// ovrHmd_BeginFrameTiming
// ovrHmd_ConfigureRendering
// ovrHmd_Create
// ovrHmd_CreateDebug
// ovrHmd_CreateDistortionMesh
// ovrHmd_Destroy
// ovrHmd_DestroyDistortionMesh
// ovrHmd_Detect
// ovrHmd_EndEyeRender
// ovrHmd_EndFrame
// ovrHmd_EndFrameTiming
// ovrHmd_GetArraySize
// ovrHmd_GetDesc
// ovrHmd_GetEnabledCaps
// ovrHmd_GetEyePose
// ovrHmd_GetEyeTimewarpMatrices
// ovrHmd_GetFloat
// ovrHmd_GetFloatArray
// ovrHmd_GetFovTextureSize
// ovrHmd_GetFrameTiming
// ovrHmd_GetLastError
// ovrHmd_GetLatencyTestDrawColor
// ovrHmd_GetLatencyTestResult
// ovrHmd_GetMeasuredLatencyTest2
// ovrHmd_GetRenderDesc
// ovrHmd_GetRenderScaleAndOffset
// ovrHmd_GetSensorDesc
// ovrHmd_GetSensorState
// ovrHmd_GetString
// ovrHmd_ResetFrameTiming
// ovrHmd_ResetSensor
// ovrHmd_SetEnabledCaps
// ovrHmd_SetFloat
// ovrHmd_SetFloatArray
// ovrHmd_StartSensor
// ovrHmd_StopSensor
// ovrMatrix4f_OrthoSubProjection
// ovrMatrix4f_Projection

#include "HookAI.h"

// Provides access to the Nektra cHookMgr
#include "DLLMainHook.h"
#include "log.h"

#include <dxgi.h>
#include <vector>

// Include the OVR header file directly that will resolve
// ovr API constants.  The LibOVR is not part of the build.
#include "LibOVR\Include\OVR_CAPI_D3D.h"

// Include the OpenVR header file, which allows us to build
// against the expected runtime of OpenVR.
//#include "openvr.h"



// Log every 100 frames
static unsigned int m_frame;

#define Log100(fmt, ...) \
	do { if (LogFile && !(m_frame % 100)) fprintf(LogFile, fmt, __VA_ARGS__); } while (0)


//---------------------------------------------------------------------------------

static HRESULT InstallAIHook(char *func, void **trampoline, void *hook)
{
	HINSTANCE module;
	SIZE_T hook_id;
	DWORD dwOsErr;
	void *fnOrig;

	module = NktHookLibHelpers::GetModuleBaseAddress(L"AI.exe");
	if (module == NULL)
	{
		LogInfo("*** Failed to GetModuleBaseAddress for AI.exe%s\n");
		return E_FAIL;
	}

	fnOrig = NktHookLibHelpers::GetProcedureAddress(module, func);
	if (fnOrig == NULL) {
		LogInfo("*** Failed to get address of %s\n", func);
		return E_FAIL;
	}

	dwOsErr = cHookMgr.Hook(&hook_id, trampoline, fnOrig, hook);
	if (dwOsErr != ERROR_SUCCESS) {
		LogInfo("*** Failed to hook %s: 0x%x\n", func, dwOsErr);
		return E_FAIL;
	}

	LogInfo("  Successfully hooked %s: 0x%x\n", func, dwOsErr);

	return NOERROR;
}

//------------------------------------------------------------
// Stolen from the Advanced OculusTinyRoom sample, to make it easier 
// to manage the new SDK.

#ifndef VALIDATE
#define VALIDATE(x, msg) if (!(x)) { MessageBoxA(nullptr, (msg), "OculusRoomTiny", MB_ICONERROR | MB_OK); exit(-1); }
#endif

#ifndef FATALERROR
#define FATALERROR(msg) { MessageBoxA(nullptr, (msg), "OculusRoomTiny", MB_ICONERROR | MB_OK); exit(-1); }
#endif

// clean up member COM pointers
template<typename T> void Release(T *&obj)
{
	if (!obj) return;
	obj->Release();
	obj = nullptr;
}


// ovrSwapTextureSet wrapper class that also maintains the render target views
// needed for D3D11 rendering.
//------------------------------------------------------------
struct OculusTexture
{
	ovrSession               Session;
	ovrTextureSwapChain      TextureChain;
	std::vector<ID3D11RenderTargetView*> TexRtv;
	int                      SizeW, SizeH;
	std::vector<ID3D11Texture2D*> Texture2D;

	OculusTexture() :
		Session(nullptr),
		TextureChain(nullptr),
		SizeW(0),
		SizeH(0)
	{
	}

	bool Init(ovrSession session, int sizeW, int sizeH, ID3D11Device* gameDevice)
	{
		Session = session;
		SizeW = sizeW;
		SizeH = sizeH;

		ovrTextureSwapChainDesc desc = {};
		desc.Type = ovrTexture_2D;
		desc.ArraySize = 1;
//		desc.Format = OVR_FORMAT_R8G8B8A8_UNORM_SRGB;
		desc.Format = OVR_FORMAT_B8G8R8A8_UNORM;
		desc.Width = sizeW;
		desc.Height = sizeH;
		desc.MipLevels = 1;
		desc.SampleCount = 1;
		desc.StaticImage = ovrFalse;
		desc.MiscFlags = ovrTextureMisc_DX_Typeless;
//		if (isItProtectedContent) desc.MiscFlags |= ovrTextureMisc_ProtectedContent;
		desc.BindFlags = ovrTextureBind_DX_RenderTarget;

		ovrResult result = ovr_CreateTextureSwapChainDX(Session, gameDevice, &desc, &TextureChain);
		if (!OVR_SUCCESS(result))
			return false;

		int textureCount = 0;
		ovr_GetTextureSwapChainLength(Session, TextureChain, &textureCount);		// returns 3 chains.
		for (int i = 0; i < textureCount; ++i)
		{
			ID3D11Texture2D* tex = nullptr;
			ovr_GetTextureSwapChainBufferDX(Session, TextureChain, i, IID_PPV_ARGS(&tex));
			Texture2D.push_back(tex);

			D3D11_RENDER_TARGET_VIEW_DESC rtvd = {};
			rtvd.Format = DXGI_FORMAT_B8G8R8A8_UNORM; // DXGI_FORMAT_R8G8B8A8_UNORM;
			rtvd.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
			ID3D11RenderTargetView* rtv;
			gameDevice->CreateRenderTargetView(tex, &rtvd, &rtv);
			TexRtv.push_back(rtv);
			//tex->Release();
		}

		return true;
	}

	~OculusTexture()
	{
		for (int i = 0; i < (int)TexRtv.size(); ++i)
		{
			Release(TexRtv[i]);
		}
		if (TextureChain)
		{
			ovr_DestroyTextureSwapChain(Session, TextureChain);
			TextureChain = nullptr;
		}
	}

	ID3D11RenderTargetView* GetRTV()
	{
		if (TextureChain)
		{
			int currentIndex = 0;
			ovr_GetTextureSwapChainCurrentIndex(Session, TextureChain, &currentIndex);
			return TexRtv[currentIndex];
		}
		return nullptr;
	}

	// New addition, because we need access to the underlying texture to CopySubresourceRegion.
	ID3D11Texture2D* GetTexture2D()
	{
		if (TextureChain)
		{
			int currentIndex = 0;
			ovr_GetTextureSwapChainCurrentIndex(Session, TextureChain, &currentIndex);
			return Texture2D[currentIndex];
		}
		return nullptr;
	}

	void Commit()
	{
		ovr_CommitTextureSwapChain(Session, TextureChain);
	}
};



//---------------------------------------------------------------------------------
// These prototypes all match the original calls in the 0.3.2 
// Oculus SDK, so that we can be sure that parameters are passed
// correctly.  These function as a typedef as well as the storage location
// for the trampoline functions which also act as the original call.

OVR_EXPORT ovrBool(*fnOrig_ovr_Initialize)() = nullptr;
OVR_EXPORT void(*fnOrig_ovr_Shutdown)() = nullptr;
OVR_EXPORT double(*fnOrig_ovr_GetTimeInSeconds)() = nullptr;
OVR_EXPORT double(*fnOrig_ovr_WaitTillTime)(double absTime) = nullptr;

OVR_EXPORT ovrPosef(*fnOrig_ovrHmd_BeginEyeRender)(ovrHmd hmd, ovrEyeType eye) = nullptr;
OVR_EXPORT ovrFrameTiming(*fnOrig_ovrHmd_BeginFrame)(ovrHmd hmd, unsigned int frameIndex) = nullptr;
OVR_EXPORT ovrFrameTiming(*fnOrig_ovrHmd_BeginFrameTiming)(ovrHmd hmd, unsigned int frameIndex) = nullptr;
OVR_EXPORT ovrBool(*fnOrig_ovrHmd_ConfigureRendering)(ovrHmd hmd,
	const ovrRenderAPIConfig* apiConfig,
	unsigned int distortionCaps,
	const ovrFovPort eyeFovIn[2],
	ovrEyeRenderDesc eyeRenderDescOut[2]) = nullptr;
OVR_EXPORT ovrHmd(*fnOrig_ovrHmd_Create)(int index) = nullptr;
OVR_EXPORT ovrHmd(*fnOrig_ovrHmd_CreateDebug)(ovrHmdType type) = nullptr;
OVR_EXPORT ovrBool(*fnOrig_ovrHmd_CreateDistortionMesh)(ovrHmd hmd,
	ovrEyeType eyeType, ovrFovPort fov,
	unsigned int distortionCaps,
	ovrDistortionMesh *meshData) = nullptr;
OVR_EXPORT void(*fnOrig_ovrHmd_Destroy)(ovrHmd hmd) = nullptr;
OVR_EXPORT void(*fnOrig_ovrHmd_DestroyDistortionMesh)(ovrDistortionMesh* meshData) = nullptr;
OVR_EXPORT int(*fnOrig_ovrHmd_Detect)() = nullptr;
OVR_EXPORT void(*fnOrig_ovrHmd_EndEyeRender)(ovrHmd hmd, ovrEyeType eye,
	ovrPosef renderPose, ovrTexture* eyeTexture) = nullptr;
OVR_EXPORT void(*fnOrig_ovrHmd_EndFrame)(ovrHmd hmd) = nullptr;
OVR_EXPORT void(*fnOrig_ovrHmd_EndFrameTiming)(ovrHmd hmd) = nullptr;

OVR_EXPORT unsigned int(*fnOrig_ovrHmd_GetArraySize)(ovrHmd hmd, const char* propertyName) = nullptr;
OVR_EXPORT void(*fnOrig_ovrHmd_GetDesc)(ovrHmd hmd, ovrHmdDesc* desc) = nullptr;
OVR_EXPORT unsigned int(*fnOrig_ovrHmd_GetEnabledCaps)(ovrHmd hmd) = nullptr;
OVR_EXPORT ovrPosef(*fnOrig_ovrHmd_GetEyePose)(ovrHmd hmd, ovrEyeType eye) = nullptr;
OVR_EXPORT void(*fnOrig_ovrHmd_GetEyeTimewarpMatrices)(ovrHmd hmd, ovrEyeType eye,
	ovrPosef renderPose, ovrMatrix4f twmOut[2]) = nullptr;
OVR_EXPORT float(*fnOrig_ovrHmd_GetFloat)(ovrHmd hmd, const char* propertyName, float defaultVal) = nullptr;
OVR_EXPORT unsigned int(*fnOrig_ovrHmd_GetFloatArray)(ovrHmd hmd, const char* propertyName,
	float values[], unsigned int arraySize) = nullptr;
OVR_EXPORT ovrSizei(*fnOrig_ovrHmd_GetFovTextureSize)(ovrHmd hmd, ovrEyeType eye, ovrFovPort fov,
	float pixelsPerDisplayPixel) = nullptr;
OVR_EXPORT ovrFrameTiming(*fnOrig_ovrHmd_GetFrameTiming)(ovrHmd hmd, unsigned int frameIndex) = nullptr;
OVR_EXPORT const char* (*fnOrig_ovrHmd_GetLastError)(ovrHmd hmd) = nullptr;
OVR_EXPORT ovrBool(*fnOrig_ovrHmd_GetLatencyTestDrawColor)(ovrHmd hmd, unsigned char rgbColorOut[3]) = nullptr;
OVR_EXPORT const char*  (*fnOrig_ovrHmd_GetLatencyTestResult)(ovrHmd hmd) = nullptr;
OVR_EXPORT double(*fnOrig_ovrHmd_GetMeasuredLatencyTest2)(ovrHmd hmd) = nullptr;
OVR_EXPORT ovrEyeRenderDesc(*fnOrig_ovrHmd_GetRenderDesc)(ovrHmd hmd,
	ovrEyeType eyeType, ovrFovPort fov) = nullptr;
OVR_EXPORT void(*fnOrig_ovrHmd_GetRenderScaleAndOffset)(ovrFovPort fov,
	ovrSizei textureSize, ovrRecti renderViewport,
	ovrVector2f uvScaleOffsetOut[2]) = nullptr;
OVR_EXPORT ovrBool(*fnOrig_ovrHmd_GetSensorDesc)(ovrHmd hmd, ovrSensorDesc* descOut) = nullptr;
OVR_EXPORT ovrSensorState(*fnOrig_ovrHmd_GetSensorState)(ovrHmd hmd, double absTime) = nullptr;
OVR_EXPORT const char* (*fnOrig_ovrHmd_GetString)(ovrHmd hmd, const char* propertyName,
	const char* defaultVal) = nullptr;

OVR_EXPORT void(*fnOrig_ovrHmd_ResetFrameTiming)(ovrHmd hmd, unsigned int frameIndex) = nullptr;
OVR_EXPORT void(*fnOrig_ovrHmd_ResetSensor)(ovrHmd hmd) = nullptr;
OVR_EXPORT void(*fnOrig_ovrHmd_SetEnabledCaps)(ovrHmd hmd, unsigned int hmdCaps) = nullptr;
OVR_EXPORT ovrBool(*fnOrig_ovrHmd_SetFloat)(ovrHmd hmd, const char* propertyName, float value) = nullptr;
OVR_EXPORT ovrBool(*fnOrig_ovrHmd_SetFloatArray)(ovrHmd hmd, const char* propertyName,
	float values[], unsigned int arraySize) = nullptr;
OVR_EXPORT ovrBool(*fnOrig_ovrHmd_StartSensor)(ovrHmd hmd, unsigned int supportedSensorCaps,
	unsigned int requiredSensorCaps) = nullptr;
OVR_EXPORT void(*fnOrig_ovrHmd_StopSensor)(ovrHmd hmd) = nullptr;

OVR_EXPORT ovrMatrix4f(*fnOrig_ovrMatrix4f_OrthoSubProjection)(ovrMatrix4f projection, ovrVector2f orthoScale,
	float orthoDistance, float eyeViewAdjustX) = nullptr;
OVR_EXPORT ovrMatrix4f(*fnOrig_ovrMatrix4f_Projection)(ovrFovPort fov,
	float znear, float zfar, ovrBool rightHanded) = nullptr;


//---------------------------------------------------------------------------------
// All implementations of the hooked routines.  All of them will at
// least log they were called, but not all need an override to be used with OpenVR.
// 
// These were built up using various regex expressions, to avoid copy paste errors.

// The very first call made by any VR app.  We need to translate the
// ovr_Initialize into an OpenVR VRInit. 

//vr::IVRSystem *m_pHMD;
ovrSession m_pHMD;
ovrGraphicsLuid m_luid;

OVR_EXPORT ovrBool Hooked_ovr_Initialize()
{
	LogInfo("*** Hooked_ovr_Initialize called.\n");

	//vr::EVRInitError eError = vr::VRInitError_None;
	//m_pHMD = vr::VR_Init(&eError, vr::VRApplication_Scene);

	//if (eError != vr::VRInitError_None)
	//{
	//	m_pHMD = NULL;
	//	LogInfo("*** Unable to init VR runtime: %s", vr::VR_GetVRInitErrorAsEnglishDescription(eError));
	//	return false;
	//}
	ovrInitParams params = { 0 };
	params.Flags = ovrInit_Debug;
	ovrResult result = ovr_Initialize(&params);
	if (OVR_FAILURE(result))
	{
		m_pHMD = NULL;
		LogInfo("*** Unable to init Oculus runtime: %d", result);
		return false;
	}

	LogInfo("  return result: %d\n", result);

	return true;
}

OVR_EXPORT void Hooked_ovr_Shutdown()
{
	LogInfo("*** Hooked_ovr_Shutdown called.\n");

	//vr::VR_Shutdown();

	ovr_Destroy(m_pHMD);
	ovr_Shutdown();
}

OVR_EXPORT double Hooked_ovr_GetTimeInSeconds()
{
	LogInfo("*** Hooked_ovr_GetTimeInSeconds called.\n");

	return 0;
}

OVR_EXPORT double Hooked_ovr_WaitTillTime(double absTime)
{
	LogInfo("*** Hooked_ovr_Initialize called.\n");

	return 0;
}


// For these ovrHmd_ version, this complicated regex was used.
// Anything that takes several hours to figure out is worth a comment.
// This required the use of non-matching atomic group and the 
// possessive quantifier +, to handle the multi-line declarations.
//   find: ((ovrHmd_[a-zA-Z0-9]*)(?>.*+\n?){1,5}{\n.*)xxx(.*\n.*)(fnOrig_xxx)
//   replace: Hooked_$1Hooked_$2$3fnOrig_$2
// First pass fix parameter:
//	 find: ((?>Hooked_ovrHmd_[a-zA-Z0-9]+)\(.* (.+), .* (.+)[\),](?>\n?.*+){1,4}\n.*)yyy
//	 replace: $1$2, $3
// Second pass parameter fix:
//   find: ((?>Hooked_ovrHmd_[a-zA-Z0-9]*)\(.* (\w+)\)\n.*\n.*\n.*)yyy
//   replace: $1$2
// The last few were fixed manually.


// One of the primary calls made by the game.  This needs to translate
// from the old 0.3 Oculus SDK to the OpenVR SDK.
// In this case, the HMD session/device is already created, because it
// is returned by the VRInit call.  So this call just needs to use that.

OVR_EXPORT ovrHmd   Hooked_ovrHmd_Create(int index)
{
	LogInfo("*** Hooked_ovrHmd_Create called.\n");

	//LogInfo("  IVRSystem as ovrHmd: %p\n", m_pHMD);

	// Since this is an opaque structure, the game cannot use it for
	// anything other than a reference to the HMD.  If we return our
	// OpenVR one, it should work.
	//return reinterpret_cast<ovrHmd>(m_pHMD);

	ovrResult result = ovr_Create(&m_pHMD, &m_luid);
	if (OVR_FAILURE(result))
	{
		ovrErrorInfo info;
		ovr_GetLastErrorInfo(&info);
		LogInfo(" ** Failed ovr_Create: %d, %s\n", result, info.ErrorString);

		ovr_Shutdown();
		return nullptr;
	}
	
	ovrHmdDesc desc = ovr_GetHmdDesc(m_pHMD);
	LogInfo("  ProductName: %s, ManufacturerName: %s\n", desc.ProductName, desc.Manufacturer);
	LogInfo("  AvailableCaps: %x, DefaultCaps: %x\n", desc.AvailableHmdCaps, desc.DefaultHmdCaps);
	LogInfo("  AvailableTrackingCaps: %x, DefaultTrackingCaps: %x\n", desc.AvailableTrackingCaps, desc.DefaultTrackingCaps);
	LogInfo("  Resolution: %d, %d\n", desc.Resolution.w, desc.Resolution.h);

	LogInfo("->return result: %d, ovrHmd: %p\n", result, m_pHMD);

	return m_pHMD;
}



// Get the primary display IDXGIOutput
// We'll use this display as the game output, which will be copied to the Rift.
// ToDo: do something other than primary display?

IDXGIOutput* getDisplayOutput()
{
	IDXGIFactory* pFactory;
	CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)(&pFactory));

	uint32_t adapterIndex = 0;
	IDXGIAdapter* pAdapter;
	pFactory->EnumAdapters(adapterIndex, &pAdapter);

	uint32_t outputIndex = 0;
	IDXGIOutput* pOutput;
	pAdapter->EnumOutputs(outputIndex, &pOutput);

	pAdapter->Release();
	pFactory->Release();
	return pOutput;
}

// The strategy here is to just do the minimum work to get it running.
// The game is not going to use or need everything the SDK supports, 
// so there is no point in filling all that in.  This is built up by
// trial and error to see what all the game calls.
// Some of the calls are tricky though, in that the structs vary over
// the old and new SDK, even using the same names.  


static DXGI_OUTPUT_DESC outDesc;

OVR_EXPORT void     Hooked_ovrHmd_GetDesc(ovrHmd hmd, old_ovrHmdDesc* desc)
{
	LogInfo("*** Hooked_ovrHmd_GetDesc called.\n");

	memset(desc, 0, sizeof(old_ovrHmdDesc));

	ovrHmdDesc new_hmdDesc = ovr_GetHmdDesc(m_pHMD);

	// We need to fill in the pieces the game needs for sure, like the
	// expected resolution and FOV.
	desc->Handle = m_pHMD;
	desc->Type = new_hmdDesc.Type;
	desc->ProductName = new_hmdDesc.ProductName;
	desc->Manufacturer = new_hmdDesc.Manufacturer;

	//desc->HmdCaps = ovrHmdCap_Present&ovrHmdCap_Available&ovrHmdCap_LowPersistence;
	//desc->SensorCaps = ovrSensorCap_Orientation&ovrSensorCap_YawCorrection&ovrSensorCap_Position;
	//desc->DistortionCaps = ovrDistortionCap_Chromatic&ovrDistortionCap_TimeWarp;
	desc->HmdCaps = new_hmdDesc.AvailableHmdCaps;
	desc->SensorCaps = new_hmdDesc.AvailableTrackingCaps;
	desc->DistortionCaps = 0x03; // chromatic & timewarp

	//uint32_t width, height;
	//ovrSizei screen;
	//m_pHMD->GetRecommendedRenderTargetSize(&width, &height);
	//screen.w = width;
	//screen.h = height;
	//desc->Resolution = screen;
	desc->Resolution = new_hmdDesc.Resolution;
	LogInfo("  Resolution: %d, %d\n", desc->Resolution.w, desc->Resolution.h);

	//desc->WindowsPos = 0,0

	//float left, right, top, bottom;
	//m_pHMD->GetProjectionRaw(vr::Eye_Left, &left, &right, &top, &bottom);
	//desc->DefaultEyeFov[ovrEye_Left].LeftTan = left;
	//desc->DefaultEyeFov[ovrEye_Left].RightTan = right;
	//desc->DefaultEyeFov[ovrEye_Left].UpTan = top;
	//desc->DefaultEyeFov[ovrEye_Left].DownTan = bottom;
	desc->DefaultEyeFov[ovrEye_Left] = new_hmdDesc.DefaultEyeFov[ovrEye_Left];

	//desc->MaxEyeFov[ovrEye_Left] = desc->DefaultEyeFov[ovrEye_Left];
	desc->MaxEyeFov[ovrEye_Left] = new_hmdDesc.MaxEyeFov[ovrEye_Left];

	//m_pHMD->GetProjectionRaw(vr::Eye_Right, &left, &right, &top, &bottom);
	//desc->DefaultEyeFov[ovrEye_Right].LeftTan = left;
	//desc->DefaultEyeFov[ovrEye_Right].RightTan = right;
	//desc->DefaultEyeFov[ovrEye_Right].UpTan = top;
	//desc->DefaultEyeFov[ovrEye_Right].DownTan = bottom;
	desc->DefaultEyeFov[ovrEye_Right] = new_hmdDesc.DefaultEyeFov[ovrEye_Right];

	//desc->MaxEyeFov[ovrEye_Right] = desc->DefaultEyeFov[ovrEye_Right];
	desc->MaxEyeFov[ovrEye_Right] = new_hmdDesc.MaxEyeFov[ovrEye_Right];

//	LogInfo("  FOV tangents: l %f, r %f, t %f, b %f\n", left, right, top, bottom);
	
	//EyeRenderOrder = 0;

	IDXGIOutput* pOutput = getDisplayOutput();
	pOutput->GetDesc(&outDesc);

	// This typecast from wchar to char is weird, but seems to work.
	// The declaration of this DisplayDeviceName should never have been
	// char* in the first place.  The OS returns wchar*, and presumably
	// this string is used to lookup the device to render.
	// Before having this string returned, we were seeing a crash, so it
	// is fundamentally required.
	// ToDo: does it work to use wchar name?
	desc->DisplayDeviceName =   "\\\\.\\DISPLAY2"; //(char*)(outDesc.DeviceName);

	// The swapchain is forced to windowed to start at CreateSwapChain, and then
	// later the game calls SetFullScreenState, which succeeds.
	//pSwapChain->SetFullscreenState(TRUE, pOculusOutputDevice);

	// DisplayId = 0;
}

static 	ovrSizei screen;

OVR_EXPORT ovrSizei Hooked_ovrHmd_GetFovTextureSize(ovrHmd hmd, ovrEyeType eye, ovrFovPort fov,
	float pixelsPerDisplayPixel)
{
	LogInfo("*** Hooked_ovrHmd_GetFovTextureSize called.\n");
	LogInfo("  ovrHmd: %p, ovrEyeType: %d, pixelsPerDisplayPixel: %f\n", hmd, eye, pixelsPerDisplayPixel);
	LogInfo("  ovrFovPort: l %f, r %f, t %f, b %f\n", fov.LeftTan, fov.RightTan, fov.UpTan, fov.DownTan);

	//// It's not clear if this could be optimized, but for now we'll just
	//// return the same GetRecommendedRenderTargetSize.
	//uint32_t width, height;
	//m_pHMD->GetRecommendedRenderTargetSize(&width, &height);
	//screen.w = width;
	//screen.h = height;

	screen = ovr_GetFovTextureSize(m_pHMD, eye, fov, pixelsPerDisplayPixel);

	LogInfo("  Resolution: %d, %d\n", screen.w, screen.h);

	return screen;
}

OVR_EXPORT void Hooked_ovrHmd_SetEnabledCaps(ovrHmd hmd, unsigned int hmdCaps)
{
	LogInfo("*** Hooked_ovrHmd_SetEnabledCaps called.\n");
	LogInfo("  ovrHmd: %p, hmdCaps: 0x%x\n", hmd, hmdCaps);

	// The input hmdCaps is 0x180, which is a bizarre choice. Probably partly
	// in error, because it would be ovrHmdCap_LowPersistence & ovrHmdCap_LatencyTest.
	// LatencyTest makes no sense, LowPersistence probably.  
	// There is no comparable call in OpenVR, so let's just do nothing
	// and pretend it worked. Vive is always set to low persistence mode,
	// and presumably it also makes this Oculus SDK call.
}



//--------------------------------------------------------------------------------
// In order to match the original 0.3.2 API, this needs to use the old version
// of the ovrEyeRenderDesc, which is a Vector3, not a Pose.

// This ConfigureRendering is the spot where we can do Init functions for
// the new SDK. It is only called by the game once, and is kinda/sorta the
// init for the old SDK.


// Game specific DX11 items
static ID3D11Device*			gameDevice;
static ID3D11DeviceContext*     gameDeviceContext;
static ID3D11RenderTargetView*	gameBackBufferRT;
static IDXGISwapChain*          gameSwapChain;
static ID3D11Texture2D*			gameBackBuffer;

// New Oculus SDK specific items
static ovrTextureSwapChain		ovrSwapChain;
static std::vector<ID3D11RenderTargetView*> ovrRtvVector;
static ovrRecti                 EyeRenderViewport[2];    // Useful to remember when varying resolution
static ovrEyeRenderDesc         EyeRenderDesc[2];        // Description of the VR.
static ovrPosef					EyeRenderPose[2] = { 0 };
static OculusTexture*			pEyeRenderTexture[2] = { nullptr, nullptr };
static ovrLayerEyeFov			ovrLayer;
static ovrMirrorTexture         ovrMirror;

OVR_EXPORT ovrBool Hooked_ovrHmd_ConfigureRendering(ovrHmd hmd,
	const ovrRenderAPIConfig* apiConfig,
	unsigned int distortionCaps,
	const ovrFovPort eyeFovIn[2],
	old_ovrEyeRenderDesc eyeRenderDescOut[2])
{
	LogInfo("*** Hooked_ovrHmd_ConfigureRendering called.\n");
	LogInfo("  ovrHmd: %p, distortionCaps: 0x%x\n", hmd, distortionCaps);
	ovrFovPort l = eyeFovIn[ovrEye_Left];
	ovrFovPort r = eyeFovIn[ovrEye_Right];
	LogInfo("  eyeFovIn[0]: l %f, r %f, t %f, b %f\n", l.LeftTan, l.RightTan, l.UpTan, l.DownTan);
	LogInfo("  eyeFovIn[1]: l %f, r %f, t %f, b %f\n", r.LeftTan, r.RightTan, r.UpTan, r.DownTan);

	// We need to fill in the eyeRenderDescOut struct to simulate what the Oculus
	// SDK would have done.

	// This is current SDK, last parameter is slightly different from ovrVector3f to ovrPosef,
	// which is a smaller struct. Old SDK says this was translation for IPD.  It's not clear
	// if the ovrPosef.position is the same thing.
	// Eyes are inverted left/right by game, so let's setup these EyeRenderDesc opposite,
	// so that we don't need any later conversion.
	ovrEyeRenderDesc leftDesc = ovr_GetRenderDesc(m_pHMD, ovrEye_Right, eyeFovIn[ovrEye_Right]);
	ovrEyeRenderDesc rightDesc = ovr_GetRenderDesc(m_pHMD, ovrEye_Left, eyeFovIn[ovrEye_Left]);

	eyeRenderDescOut[ovrEye_Left].Eye = leftDesc.Eye;
	eyeRenderDescOut[ovrEye_Left].Fov = leftDesc.Fov;
	eyeRenderDescOut[ovrEye_Left].DistortedViewport = leftDesc.DistortedViewport;
	eyeRenderDescOut[ovrEye_Left].PixelsPerTanAngleAtCenter = leftDesc.PixelsPerTanAngleAtCenter;
	eyeRenderDescOut[ovrEye_Left].ViewAdjust = leftDesc.HmdToEyePose.Position;

	eyeRenderDescOut[ovrEye_Right].Eye = rightDesc.Eye;
	eyeRenderDescOut[ovrEye_Right].Fov = rightDesc.Fov;
	eyeRenderDescOut[ovrEye_Right].DistortedViewport = rightDesc.DistortedViewport;
	eyeRenderDescOut[ovrEye_Right].PixelsPerTanAngleAtCenter = rightDesc.PixelsPerTanAngleAtCenter;
	eyeRenderDescOut[ovrEye_Right].ViewAdjust = rightDesc.HmdToEyePose.Position;


	// That completes the return results, this next part is to set up for drawing,
	// using the new SDK. Sample code creates layer struct at each frame, 
	// but docs suggest that it should be done once at init time.
	// This is the best time, because the swapChain has been passed in
	// by the game in the ovrRenderConfigAPI


	// Initialize our single full screen Fov layer.
	// The mapping from old SDK to new is not at all clear.  It's not
	// obvious whether an ovrTextureSwapChain can be just an IDXGISwapChain
	// like it was defined in 0.3.2.

	// Save off all the apiConfig elements, because these are the pieces that
	// the game itself is going to render to, where the actual game bits live.

	if (apiConfig == nullptr)		// Can be null in Debug builds.
	{
		LogInfo(" ** Failed Hooked_ovrHmd_ConfigureRendering, apiConfig is null.\n");
		return ovrFalse;
	}

	// Game specific DX11 items
	const ovrD3D11Config* d3d11 = reinterpret_cast<const ovrD3D11Config*>(apiConfig);
	gameDevice = d3d11->D3D11.pDevice;
	gameDeviceContext = d3d11->D3D11.pDeviceContext;
	gameBackBufferRT = d3d11->D3D11.pBackBufferRT;
	gameSwapChain = d3d11->D3D11.pSwapChain;
	gameSwapChain->GetBuffer(0, IID_PPV_ARGS(&gameBackBuffer));		

	D3D11_TEXTURE2D_DESC windDesc;		// 2688 by 1600
	gameBackBuffer->GetDesc(&windDesc);
	LogInfo("  window texture w: %d, h: %d, format: %d\n", windDesc.Width, windDesc.Height, windDesc.Format);


	// New Oculus SDK specific items
	// Build up the pieces we need for new Oculus SDK drawing.  These are all part of
	// the current SDK, and where we'll copy the game bits, so that they show in Rift.
	//
	// Init sequence for Rift drawing, stolen from Advanced OculusTinyRoom sample.  
	// Changed as little as feasible.  This sets up the OculusTexture, which is then 
	// connected to the output layer.  At EndFrame, we'll copy the game data into
	// the OculusTexture, and present it to the Rift. This takes the place of the
	// Render sequence in the sample.
	{
		ovrSession Session = m_pHMD;

		//void MakeEyeBuffers(float pixelsPerDisplayPixel = 1.0f, bool isItProtectedContent = false)
		{
			for (int eye = 0; eye < 2; ++eye)
			{
				ovrSizei idealSize = ovr_GetFovTextureSize(Session, (ovrEyeType)eye, ovr_GetHmdDesc(Session).DefaultEyeFov[eye], 1.0f);
				pEyeRenderTexture[eye] = new OculusTexture();
				if (!pEyeRenderTexture[eye]->Init(Session, idealSize.w, idealSize.h, gameDevice))
					return ovrFalse;
				//pEyeDepthBuffer[eye] = new DepthBuffer(DIRECTX.Device, idealSize.w, idealSize.h);
				EyeRenderViewport[eye].Pos.x = 0;
				EyeRenderViewport[eye].Pos.y = 0;
				EyeRenderViewport[eye].Size = idealSize;
			}
		}
		//void ConfigureRendering(const ovrFovPort * fov = 0)
		{
			// If any values are passed as NULL, then we use the default basic case
			//if (!fov) 
			ovrFovPort*	fov = ovr_GetHmdDesc(Session).DefaultEyeFov;
			EyeRenderDesc[0] = ovr_GetRenderDesc(Session, ovrEye_Left, fov[0]);
			EyeRenderDesc[1] = ovr_GetRenderDesc(Session, ovrEye_Right, fov[1]);
		}


		// Create a mirror to see on the monitor.
		ovrMirrorTextureDesc mirrorDesc = {};
		mirrorDesc.Format = OVR_FORMAT_B8G8R8A8_UNORM_SRGB;
		mirrorDesc.Width = 2688;
		mirrorDesc.Height = 1600;
		mirrorDesc.MirrorOptions = ovrMirrorOption_Default;
		ovr_CreateMirrorTextureWithOptionsDX(m_pHMD, gameDevice, &mirrorDesc, &ovrMirror);
	}

	LogInfo("  Created OculusTexture with D3D11Device: %p, D3D11Context: %p, DXGISwapChain: %p, w: %d, h: %d\n",
		gameDevice, gameDeviceContext, gameSwapChain, 2688, 1600);

	return ovrTrue;
}


//typedef enum
//{
//	ovrSensorCap_Orientation = 0x0010,   //  Supports orientation tracking (IMU).
//	ovrSensorCap_YawCorrection = 0x0020,   //  Supports yaw correction through magnetometer or other means.
//	ovrSensorCap_Position = 0x0040,   //  Supports positional tracking.

OVR_EXPORT ovrBool  Hooked_ovrHmd_StartSensor(ovrHmd hmd, unsigned int supportedSensorCaps, 
	unsigned int requiredSensorCaps)
{
	LogInfo("*** Hooked_ovrHmd_StartSensor called.\n");
	LogInfo("  supportedSensorCaps: %x, requiredSensorCaps: %x\n", supportedSensorCaps, requiredSensorCaps);

	// Doesn't really matter what the inputs are, let's just say it works.
	// Current SDK and CV1/DK2 support all these flags.
	return ovrTrue;
}


static double lastFrameSeconds = 0.0;
static ovrFrameTiming frameTiming;		// probably unnecessary, copy is made on exit.

OVR_EXPORT ovrFrameTiming Hooked_ovrHmd_GetFrameTiming(ovrHmd hmd, unsigned int frameIndex)
{
	Log100("*** Hooked_ovrHmd_GetFrameTiming called.\n");
	Log100("  frameIndex: %d\n", frameIndex);

	// For this ovrFrameTiming struct, the new SDK doesn't return all this
	// stuff anymore. Not clear how important these are, they only matter if
	// the game cares.

	double now = ovr_GetTimeInSeconds();
	double predictedTime = ovr_GetPredictedDisplayTime(m_pHMD, frameIndex);

	frameTiming.DeltaSeconds = (float)(now - lastFrameSeconds);
	lastFrameSeconds = now;
	Log100("  now: %f, predictedTime: %f, DeltaSeconds: %f\n", now, predictedTime, frameTiming.DeltaSeconds);


	//ToDo: Not sure these make sense, but it's all we've got. Pretty sure
	// the game does not use these.
	frameTiming.ThisFrameSeconds = now;
	frameTiming.TimewarpPointSeconds = predictedTime;
	frameTiming.NextFrameSeconds = now + frameTiming.DeltaSeconds;

	frameTiming.ScanoutMidpointSeconds = predictedTime;

	frameTiming.EyeScanoutSeconds[ovrEye_Left] = predictedTime;
	frameTiming.EyeScanoutSeconds[ovrEye_Right] = predictedTime;

	return frameTiming;
}



OVR_EXPORT ovrSensorState Hooked_ovrHmd_GetSensorState(ovrHmd hmd, double absTime)
{
	Log100("*** Hooked_ovrHmd_GetSensorState called.\n");
	Log100("  absTime: %f\n", absTime);

	// New SDK uses GetTrackingState. 
	ovrTrackingState predictedState = ovr_GetTrackingState(m_pHMD, absTime, ovrTrue);
	ovrTrackingState currentState = ovr_GetTrackingState(m_pHMD, 0.0, ovrTrue);

	ovrSensorState oldSDKState = { 0 };

	// Cannot directly assign the old SDK ovrPoseStatef, to the new one, because
	// they changed the packing by adding a 4 byte pad, making it bigger.
	oldSDKState.Predicted.Pose = predictedState.HeadPose.ThePose;
	oldSDKState.Predicted.AngularVelocity = predictedState.HeadPose.AngularVelocity;
	oldSDKState.Predicted.LinearVelocity = predictedState.HeadPose.LinearVelocity;
	oldSDKState.Predicted.AngularAcceleration = predictedState.HeadPose.AngularAcceleration;
	oldSDKState.Predicted.LinearAcceleration = predictedState.HeadPose.LinearAcceleration;
	oldSDKState.Predicted.TimeInSeconds = predictedState.HeadPose.TimeInSeconds;

	// Actual recorded pose configuration based on the sensor sample at a 
	// moment closest to the requested time.
	oldSDKState.Recorded.Pose = currentState.HeadPose.ThePose;
	oldSDKState.Recorded.AngularVelocity = currentState.HeadPose.AngularVelocity;
	oldSDKState.Recorded.LinearVelocity = currentState.HeadPose.LinearVelocity;
	oldSDKState.Recorded.AngularAcceleration = currentState.HeadPose.AngularAcceleration;
	oldSDKState.Recorded.LinearAcceleration = currentState.HeadPose.LinearAcceleration;
	oldSDKState.Recorded.TimeInSeconds = currentState.HeadPose.TimeInSeconds;

	// Sensor temperature reading, in degrees Celsius, as sample time.
	// Does not seem available in current SDK.  Room temp?
	oldSDKState.Temperature = 23.0;

	// Sensor status described by ovrStatusBits.
	// ToDo: seemed to always be zeros.  ovrStatusBits should be orientation, position tracked.
	oldSDKState.StatusFlags = currentState.StatusFlags;
	
	return oldSDKState;
}

// These next four are the primary calls done in the main loop.
// Logging will be excessive for these.

static double beginFrameSeconds = 0.0;
static double sensorSampleTime;    // sensorSampleTime is fed into the layer later

OVR_EXPORT ovrFrameTiming Hooked_ovrHmd_BeginFrame(ovrHmd hmd, unsigned int frameIndex)
{
	Log100("*** Hooked_ovrHmd_BeginFrame called.\n");
	Log100("  frameIndex: %d\n", frameIndex);

	m_frame = frameIndex;		// Needed for EndFrame

	// Follow code from OculusTinyRoom sample. This happens right before
	// the sample does rendering. Probably unnecessary, but's it not fully
	// clear what is required in new SDK, because documentation no longer matches.
	ovrSession Session = m_pHMD;

	//ovrTrackingState GetEyePoses(ovrPosef * useEyeRenderPose = 0, float * scaleIPD = 0, float * newIPD = 0)
	{
		// Get both eye poses simultaneously, with IPD offset already included. 
		ovrPosef useHmdToEyePose[2] = { EyeRenderDesc[0].HmdToEyePose,
			EyeRenderDesc[1].HmdToEyePose };

		// ...

		double ftiming = ovr_GetPredictedDisplayTime(Session, 0);
		ovrTrackingState trackingState = ovr_GetTrackingState(Session, ftiming, ovrTrue);

		ovr_CalcEyePoses(trackingState.HeadPose.ThePose, useHmdToEyePose, EyeRenderPose);
	}


	// Figure out the ovrFrameTiming for the old SDK and game to use.
	double now = ovr_GetTimeInSeconds();
	double predictedTime = ovr_GetPredictedDisplayTime(m_pHMD, frameIndex);

	ovrFrameTiming beginTiming;

	beginTiming.DeltaSeconds = (float)(now - beginFrameSeconds);
	beginFrameSeconds = now;
	Log100("  now: %f, predictedTime: %f, DeltaSeconds: %f\n", now, predictedTime, beginTiming.DeltaSeconds);

	// ToDo: best guess here.  Can be improved if necessary, by capturing
	// times from EndFrame.
	beginTiming.ThisFrameSeconds = now;
	beginTiming.TimewarpPointSeconds = predictedTime;
	beginTiming.NextFrameSeconds = now + beginTiming.DeltaSeconds;

	// Time when when half of the screen will be scanned out. Can be passed as a prediction
	// value to ovrHmd_GetSensorState() to get general orientation.
	beginTiming.ScanoutMidpointSeconds = predictedTime;
	beginTiming.EyeScanoutSeconds[ovrEye_Left] = predictedTime;
	beginTiming.EyeScanoutSeconds[ovrEye_Right] = predictedTime;

	return beginTiming;
}


OVR_EXPORT ovrPosef Hooked_ovrHmd_BeginEyeRender(ovrHmd hmd, ovrEyeType eye)
{
	Log100("*** Hooked_ovrHmd_BeginEyeRender called.\n");

	// In sample, this does both eyes at once, here we'll get called twice,
	// once for each eye.
	{
		// Unnecessary when we use CopySubresourceRegion to draw.
		//DIRECTX.SetAndClearRenderTarget(pEyeRenderTexture[eye]->GetRTV(), pEyeRenderTexture[eye]->GetDSV());
		//DIRECTX.SetViewport((float)eyeRenderViewport[eye].Pos.x, (float)eyeRenderViewport[eye].Pos.y,
		//	(float)eyeRenderViewport[eye].Size.w, (float)eyeRenderViewport[eye].Size.h);
	}

	// Return current tracking state because game likely needs it.
	double now = ovr_GetTimeInSeconds();
	ovrTrackingState predictedState = ovr_GetTrackingState(m_pHMD, now, ovrTrue);

	return predictedState.HeadPose.ThePose;
}

static ovrD3D11TextureData* dx11TexData;

OVR_EXPORT void     Hooked_ovrHmd_EndEyeRender(ovrHmd hmd, ovrEyeType eye,
	ovrPosef renderPose, ovrTexture* eyeTexture)
{
	Log100("*** Hooked_ovrHmd_EndEyeRender called.\n");

	// Copy the data from the game in eyeTexture, into our new SDK texture,
	// which is gameLayer.ColorTexture.  This will be once per eye, for each
	// half of the ColorTexture.  This uses CopySubresourceRegion, from the
	// Context the game passed in during ConfigureRendering, and the gameTexture
	// which is the ID3D11Texture2D for both eyes.

	// Sample does sceneToRender->Render(&prod, red, green, blue, alpha, true);
	{
		dx11TexData = reinterpret_cast<ovrD3D11TextureData*>(eyeTexture);
		D3D11_TEXTURE2D_DESC eyeDesc, gameDesc;
		dx11TexData->pTexture->GetDesc(&eyeDesc);			// DXGI_FORMAT_B8G8R8A8_TYPELESS
		gameBackBuffer->GetDesc(&gameDesc);

		// Source data from game is dx11TexData->pTexture, and is 2x width.
		// Destination is pEyeRenderTexture[eye]->tex, and is per eye.

		D3D11_BOX srcBox;
		srcBox.left = (eye == ovrEye_Left) ? 0 : pEyeRenderTexture[eye]->SizeW;
		srcBox.top = 0;
		srcBox.front = 0;
		srcBox.right = srcBox.left + pEyeRenderTexture[eye]->SizeW;
		srcBox.bottom = pEyeRenderTexture[eye]->SizeH;
		srcBox.back = 1;		// must be different than front.

		gameDeviceContext->CopySubresourceRegion(pEyeRenderTexture[eye]->GetTexture2D(), 0, 0, 0, 0, dx11TexData->pTexture, 0, &srcBox);
	}

	// Sample does pEyeRenderTexture[eye]->Commit();
	pEyeRenderTexture[eye]->Commit();
}



static double endFrameSeconds = 0.0;

OVR_EXPORT void     Hooked_ovrHmd_EndFrame(ovrHmd hmd)
{
	Log100("*** Hooked_ovrHmd_EndFrame called.\n");

	// Set up the output layer here during EndFrame. From Advanced TinyRoom sample:
	//void PrepareLayerHeader(OculusTexture * leftEyeTexture = 0, ovrPosef * leftEyePose = 0, XMVECTOR * extraQuat = 0)
	{
		OculusTexture *   useEyeTexture[2] = { pEyeRenderTexture[0], pEyeRenderTexture[1] };
		ovrPosef    useEyeRenderPose[2] = { EyeRenderPose[0], EyeRenderPose[1] }; 

		// ...

		ovrLayer.Header.Type = ovrLayerType_EyeFov;
		ovrLayer.Header.Flags = 0;
		ovrLayer.ColorTexture[0] = useEyeTexture[0]->TextureChain;
		ovrLayer.ColorTexture[1] = useEyeTexture[1]->TextureChain;
		ovrLayer.RenderPose[0] = useEyeRenderPose[0];
		ovrLayer.RenderPose[1] = useEyeRenderPose[1];
		ovrLayer.Fov[0] = EyeRenderDesc[0].Fov;
		ovrLayer.Fov[1] = EyeRenderDesc[1].Fov;
		ovrLayer.Viewport[0] = EyeRenderViewport[0];
		ovrLayer.Viewport[1] = EyeRenderViewport[1];
	}

	// For EndFrame type work, the OculusTinyRoom sample finalizes the image
	// by setting up the layer, and submitting it to the SDK>
	// Initialize our single full screen Fov layer.

	ovrLayerHeader* layers = &ovrLayer.Header;
	ovr_SubmitFrame(m_pHMD, m_frame, nullptr, &layers, 1);


	// Render to output as mirror window.
	// Top half is for game eye textures returned in dx11TexData
	// Setup as a single texture, 2x width.
	D3D11_TEXTURE2D_DESC eyeDesc;
	dx11TexData->pTexture->GetDesc(&eyeDesc);	// DXGI_FORMAT_B8G8R8A8_TYPELESS

	// Source the center 800 pixels from the game image.
	D3D11_BOX srcBox = { 0, (1600-800)/2, 0, 2688, (1600-800)/2 + (1600-800), 1 };
	gameDeviceContext->CopySubresourceRegion(gameBackBuffer, 0, 0, 0, 0, dx11TexData->pTexture, 0, &srcBox);


	// Bottom half is for mirror texture, that shows what is seen in Rift.
	ID3D11Texture2D* tex2D;
	ovr_GetMirrorTextureBufferDX(m_pHMD, ovrMirror, IID_PPV_ARGS(&tex2D));

	D3D11_TEXTURE2D_DESC bbDesc;
	D3D11_TEXTURE2D_DESC mirrorDesc;
	gameBackBuffer->GetDesc(&bbDesc);			// DXGI_FORMAT_R8G8B8A8_UNORM
	tex2D->GetDesc(&mirrorDesc);

	gameDeviceContext->CopySubresourceRegion(gameBackBuffer, 0, 0, (1600-800), 0, tex2D, 0, &srcBox);
	
	tex2D->Release();
	

	gameSwapChain->Present(0, 0);
}





//---------------------------------------------------------------------------------
// The remainder of these calls are imported by the game, but don't seem to ever
// be called, so there is no point in implementing them.  Just log, in case we
// ever see them.



  OVR_EXPORT ovrFrameTiming Hooked_ovrHmd_BeginFrameTiming(ovrHmd hmd, unsigned int frameIndex)
{
	LogInfo("*** Hooked_ovrHmd_BeginFrameTiming called.\n");

	   
	ovrFrameTiming zeroed = { 0 };
	return zeroed;
}

OVR_EXPORT ovrHmd   Hooked_ovrHmd_CreateDebug(ovrHmdType type)
{
	LogInfo("*** Hooked_ovrHmd_CreateDebug called.\n");

	return 0;
}

OVR_EXPORT ovrBool  Hooked_ovrHmd_CreateDistortionMesh(ovrHmd hmd,
	ovrEyeType eyeType, ovrFovPort fov,
	unsigned int distortionCaps,
	ovrDistortionMesh *meshData)
{
	LogInfo("*** Hooked_ovrHmd_CreateDistortionMesh called.\n");

	return 0;
}

OVR_EXPORT void     Hooked_ovrHmd_Destroy(ovrHmd hmd)
{
	LogInfo("*** Hooked_ovrHmd_Destroy called.\n");
}

OVR_EXPORT void     Hooked_ovrHmd_DestroyDistortionMesh(ovrDistortionMesh* meshData)
{
	LogInfo("*** Hooked_ovrHmd_DestroyDistortionMesh called.\n");
}

OVR_EXPORT int      Hooked_ovrHmd_Detect()
{
	LogInfo("*** Hooked_ovrHmd_Detect called.\n");

	return 0;
}

OVR_EXPORT void     Hooked_ovrHmd_EndFrameTiming(ovrHmd hmd)
{
	LogInfo("*** Hooked_ovrHmd_EndFrameTiming called.\n");
}


OVR_EXPORT unsigned int Hooked_ovrHmd_GetArraySize(ovrHmd hmd, const char* propertyName)
{
	LogInfo("*** Hooked_ovrHmd_GetArraySize called.\n");

	return 0;
}

OVR_EXPORT unsigned int Hooked_ovrHmd_GetEnabledCaps(ovrHmd hmd)
{
	LogInfo("*** Hooked_ovrHmd_GetEnabledCaps called.\n");

	return 0;
}

OVR_EXPORT ovrPosef Hooked_ovrHmd_GetEyePose(ovrHmd hmd, ovrEyeType eye)
{
	LogInfo("*** Hooked_ovrHmd_GetEyePose called.\n");

	ovrPosef zeroed = { 0 };
	return zeroed;
}

OVR_EXPORT void     Hooked_ovrHmd_GetEyeTimewarpMatrices(ovrHmd hmd, ovrEyeType eye,
	ovrPosef renderPose, ovrMatrix4f twmOut[2])
{
	LogInfo("*** Hooked_ovrHmd_GetEyeTimewarpMatrices called.\n");
}

OVR_EXPORT float       Hooked_ovrHmd_GetFloat(ovrHmd hmd, const char* propertyName, float defaultVal)
{
	LogInfo("*** Hooked_ovrHmd_GetFloat called.\n");

	return 0;
}

OVR_EXPORT unsigned int Hooked_ovrHmd_GetFloatArray(ovrHmd hmd, const char* propertyName,
	float values[], unsigned int arraySize)
{
	LogInfo("*** Hooked_ovrHmd_GetFloatArray called.\n");

	return 0;
}

OVR_EXPORT const char* Hooked_ovrHmd_GetLastError(ovrHmd hmd)
{
	LogInfo("*** Hooked_ovrHmd_GetLastError called.\n");

	return 0;
}

OVR_EXPORT ovrBool Hooked_ovrHmd_GetLatencyTestDrawColor(ovrHmd hmd, unsigned char rgbColorOut[3])
{
	LogInfo("*** Hooked_ovrHmd_GetLatencyTestDrawColor called.\n");

	return 0;
}

OVR_EXPORT const char*  Hooked_ovrHmd_GetLatencyTestResult(ovrHmd hmd)
{
	LogInfo("*** Hooked_ovrHmd_GetLatencyTestResult called.\n");

	return 0;
}

OVR_EXPORT double       Hooked_ovrHmd_GetMeasuredLatencyTest2(ovrHmd hmd)
{
	LogInfo("*** Hooked_ovrHmd_GetMeasuredLatencyTest2 called.\n");

	return 0;
}

OVR_EXPORT ovrEyeRenderDesc Hooked_ovrHmd_GetRenderDesc(ovrHmd hmd,
	ovrEyeType eyeType, ovrFovPort fov)
{
	LogInfo("*** Hooked_ovrHmd_GetRenderDesc called.\n");

	ovrEyeRenderDesc zeroed;
	memset(&zeroed, 0, sizeof(ovrEyeRenderDesc));
	return zeroed;
}

OVR_EXPORT void     Hooked_ovrHmd_GetRenderScaleAndOffset(ovrFovPort fov,
	ovrSizei textureSize, ovrRecti renderViewport,
	ovrVector2f uvScaleOffsetOut[2])
{
	LogInfo("*** Hooked_ovrHmd_GetRenderScaleAndOffset called.\n");
}

OVR_EXPORT ovrBool     Hooked_ovrHmd_GetSensorDesc(ovrHmd hmd, ovrSensorDesc* descOut)
{
	LogInfo("*** Hooked_ovrHmd_GetSensorDesc called.\n");

	return 0;
}

OVR_EXPORT const char* Hooked_ovrHmd_GetString(ovrHmd hmd, const char* propertyName,
	const char* defaultVal)
{
	LogInfo("*** Hooked_ovrHmd_GetString called.\n");

	return 0;
}


OVR_EXPORT void     Hooked_ovrHmd_ResetFrameTiming(ovrHmd hmd, unsigned int frameIndex)
{
	LogInfo("*** Hooked_ovrHmd_ResetFrameTiming called.\n");
}

OVR_EXPORT void     Hooked_ovrHmd_ResetSensor(ovrHmd hmd)
{
	LogInfo("*** Hooked_ovrHmd_ResetSensor called.\n");
}

OVR_EXPORT ovrBool      Hooked_ovrHmd_SetFloat(ovrHmd hmd, const char* propertyName, float value)
{
	LogInfo("*** Hooked_ovrHmd_SetFloat called.\n");

	return 0;
}

OVR_EXPORT ovrBool      Hooked_ovrHmd_SetFloatArray(ovrHmd hmd, const char* propertyName,
	float values[], unsigned int arraySize)
{
	LogInfo("*** Hooked_ovrHmd_SetFloatArray called.\n");

	return 0;
}

OVR_EXPORT void     Hooked_ovrHmd_StopSensor(ovrHmd hmd)
{
	LogInfo("*** Hooked_ovrHmd_StopSensor called.\n");
}


OVR_EXPORT ovrMatrix4f Hooked_ovrMatrix4f_OrthoSubProjection(ovrMatrix4f projection, ovrVector2f orthoScale,
	float orthoDistance, float eyeViewAdjustX)
{
	LogInfo("*** Hooked_ovrMatrix4f_OrthoSubProjection called.\n");

	ovrMatrix4f zeroed = { 0 };
	return zeroed;
}

OVR_EXPORT ovrMatrix4f Hooked_ovrMatrix4f_Projection(ovrFovPort fov,
	float znear, float zfar, ovrBool rightHanded)
{
	LogInfo("*** Hooked_ovrMatrix4f_Projection called.\n");

	ovrMatrix4f zeroed = { 0 };
	return zeroed;
}



//---------------------------------------------------------------------------------

void HookAI()
{
	LogInfo("*** Hooking all ovr_* functions in AI.exe\n");

	InstallAIHook("ovr_GetTimeInSeconds", (void**)&fnOrig_ovr_GetTimeInSeconds, Hooked_ovr_GetTimeInSeconds);
	InstallAIHook("ovr_Initialize", (void**)&fnOrig_ovr_Initialize, Hooked_ovr_Initialize);
	InstallAIHook("ovr_Shutdown", (void**)&fnOrig_ovr_Shutdown, Hooked_ovr_Shutdown);
	InstallAIHook("ovr_WaitTillTime", (void**)&fnOrig_ovr_WaitTillTime, Hooked_ovr_WaitTillTime);

	InstallAIHook("ovrHmd_BeginEyeRender", (void**)&fnOrig_ovrHmd_BeginEyeRender, Hooked_ovrHmd_BeginEyeRender);
	InstallAIHook("ovrHmd_BeginFrame", (void**)&fnOrig_ovrHmd_BeginFrame, Hooked_ovrHmd_BeginFrame);
	InstallAIHook("ovrHmd_BeginFrameTiming", (void**)&fnOrig_ovrHmd_BeginFrameTiming, Hooked_ovrHmd_BeginFrameTiming);
	InstallAIHook("ovrHmd_ConfigureRendering", (void**)&fnOrig_ovrHmd_ConfigureRendering, Hooked_ovrHmd_ConfigureRendering);
	InstallAIHook("ovrHmd_Create", (void**)&fnOrig_ovrHmd_Create, Hooked_ovrHmd_Create);
	InstallAIHook("ovrHmd_CreateDebug", (void**)&fnOrig_ovrHmd_CreateDebug, Hooked_ovrHmd_CreateDebug);
	InstallAIHook("ovrHmd_CreateDistortionMesh", (void**)&fnOrig_ovrHmd_CreateDistortionMesh, Hooked_ovrHmd_CreateDistortionMesh);
	InstallAIHook("ovrHmd_Destroy", (void**)&fnOrig_ovrHmd_Destroy, Hooked_ovrHmd_Destroy);
	InstallAIHook("ovrHmd_DestroyDistortionMesh", (void**)&fnOrig_ovrHmd_DestroyDistortionMesh, Hooked_ovrHmd_DestroyDistortionMesh);
	InstallAIHook("ovrHmd_Detect", (void**)&fnOrig_ovrHmd_Detect, Hooked_ovrHmd_Detect);
	InstallAIHook("ovrHmd_EndEyeRender", (void**)&fnOrig_ovrHmd_EndEyeRender, Hooked_ovrHmd_EndEyeRender);
	InstallAIHook("ovrHmd_EndFrame", (void**)&fnOrig_ovrHmd_EndFrame, Hooked_ovrHmd_EndFrame);
	InstallAIHook("ovrHmd_EndFrameTiming", (void**)&fnOrig_ovrHmd_EndFrameTiming, Hooked_ovrHmd_EndFrameTiming);
	InstallAIHook("ovrHmd_GetArraySize", (void**)&fnOrig_ovrHmd_GetArraySize, Hooked_ovrHmd_GetArraySize);
	InstallAIHook("ovrHmd_GetDesc", (void**)&fnOrig_ovrHmd_GetDesc, Hooked_ovrHmd_GetDesc);
	InstallAIHook("ovrHmd_GetEnabledCaps", (void**)&fnOrig_ovrHmd_GetEnabledCaps, Hooked_ovrHmd_GetEnabledCaps);
	InstallAIHook("ovrHmd_GetEyePose", (void**)&fnOrig_ovrHmd_GetEyePose, Hooked_ovrHmd_GetEyePose);
	InstallAIHook("ovrHmd_GetEyeTimewarpMatrices", (void**)&fnOrig_ovrHmd_GetEyeTimewarpMatrices, Hooked_ovrHmd_GetEyeTimewarpMatrices);
	InstallAIHook("ovrHmd_GetFloat", (void**)&fnOrig_ovrHmd_GetFloat, Hooked_ovrHmd_GetFloat);
	InstallAIHook("ovrHmd_GetFloatArray", (void**)&fnOrig_ovrHmd_GetFloatArray, Hooked_ovrHmd_GetFloatArray);
	InstallAIHook("ovrHmd_GetFovTextureSize", (void**)&fnOrig_ovrHmd_GetFovTextureSize, Hooked_ovrHmd_GetFovTextureSize);
	InstallAIHook("ovrHmd_GetFrameTiming", (void**)&fnOrig_ovrHmd_GetFrameTiming, Hooked_ovrHmd_GetFrameTiming);
	InstallAIHook("ovrHmd_GetLastError", (void**)&fnOrig_ovrHmd_GetLastError, Hooked_ovrHmd_GetLastError);
	InstallAIHook("ovrHmd_GetLatencyTestDrawColor", (void**)&fnOrig_ovrHmd_GetLatencyTestDrawColor, Hooked_ovrHmd_GetLatencyTestDrawColor);
	InstallAIHook("ovrHmd_GetLatencyTestResult", (void**)&fnOrig_ovrHmd_GetLatencyTestResult, Hooked_ovrHmd_GetLatencyTestResult);
	InstallAIHook("ovrHmd_GetMeasuredLatencyTest2", (void**)&fnOrig_ovrHmd_GetMeasuredLatencyTest2, Hooked_ovrHmd_GetMeasuredLatencyTest2);
	InstallAIHook("ovrHmd_GetRenderDesc", (void**)&fnOrig_ovrHmd_GetRenderDesc, Hooked_ovrHmd_GetRenderDesc);
	InstallAIHook("ovrHmd_GetRenderScaleAndOffset", (void**)&fnOrig_ovrHmd_GetRenderScaleAndOffset, Hooked_ovrHmd_GetRenderScaleAndOffset);
	InstallAIHook("ovrHmd_GetSensorDesc", (void**)&fnOrig_ovrHmd_GetSensorDesc, Hooked_ovrHmd_GetSensorDesc);
	InstallAIHook("ovrHmd_GetSensorState", (void**)&fnOrig_ovrHmd_GetSensorState, Hooked_ovrHmd_GetSensorState);
	InstallAIHook("ovrHmd_GetString", (void**)&fnOrig_ovrHmd_GetString, Hooked_ovrHmd_GetString);
	InstallAIHook("ovrHmd_ResetFrameTiming", (void**)&fnOrig_ovrHmd_ResetFrameTiming, Hooked_ovrHmd_ResetFrameTiming);
	InstallAIHook("ovrHmd_ResetSensor", (void**)&fnOrig_ovrHmd_ResetSensor, Hooked_ovrHmd_ResetSensor);
	InstallAIHook("ovrHmd_SetEnabledCaps", (void**)&fnOrig_ovrHmd_SetEnabledCaps, Hooked_ovrHmd_SetEnabledCaps);
	InstallAIHook("ovrHmd_SetFloat", (void**)&fnOrig_ovrHmd_SetFloat, Hooked_ovrHmd_SetFloat);
	InstallAIHook("ovrHmd_SetFloatArray", (void**)&fnOrig_ovrHmd_SetFloatArray, Hooked_ovrHmd_SetFloatArray);
	InstallAIHook("ovrHmd_StartSensor", (void**)&fnOrig_ovrHmd_StartSensor, Hooked_ovrHmd_StartSensor);
	InstallAIHook("ovrHmd_StopSensor", (void**)&fnOrig_ovrHmd_StopSensor, Hooked_ovrHmd_StopSensor);

	InstallAIHook("ovrMatrix4f_OrthoSubProjection", (void**)&fnOrig_ovrMatrix4f_OrthoSubProjection, Hooked_ovrMatrix4f_OrthoSubProjection);
	InstallAIHook("ovrMatrix4f_Projection", (void**)&fnOrig_ovrMatrix4f_Projection, Hooked_ovrMatrix4f_Projection);
}

