#include <d3d11.h>

#include "LibOVR\Include\OVR_CAPI.h"

void HookAI();


// Copy/Paste from 0.3.2 SDK to make these game APIs identical.

// Renamed in new SDK, points to same hmdStruct
// old: typedef struct ovrHmdStruct* ovrHmd;
// new: typedef struct ovrHmdStruct* ovrSession;
typedef ovrSession ovrHmd;

// This ovrHmdDesc uses the same name, but has been modified from SDKS.

// This is a complete descriptor of the HMD.
typedef struct old_ovrHmdDesc_
{
	ovrHmd      Handle;  // Handle of this HMD.
	ovrHmdType  Type;

	// Name string describing the product: "Oculus Rift DK1", etc.
	const char* ProductName;
	const char* Manufacturer;

	// Capability bits described by ovrHmdCaps.
	unsigned int HmdCaps;
	// Capability bits described by ovrSensorCaps.
	unsigned int SensorCaps;
	// Capability bits described by ovrDistortionCaps.
	unsigned int DistortionCaps;

	// Resolution of the entire HMD screen (for both eyes) in pixels.
	ovrSizei    Resolution;
	// Where monitor window should be on screen or (0,0).
	ovrVector2i WindowsPos;

	// These define the recommended and maximum optical FOVs for the HMD.    
	ovrFovPort  DefaultEyeFov[ovrEye_Count];
	ovrFovPort  MaxEyeFov[ovrEye_Count];

	// Preferred eye rendering order for best performance.
	// Can help reduce latency on sideways-scanned screens.
	ovrEyeType  EyeRenderOrder[ovrEye_Count];

	// Display that HMD should present on.
	// TBD: It may be good to remove this information relying on WidowPos instead.
	// Ultimately, we may need to come up with a more convenient alternative,
	// such as a API-specific functions that return adapter, ot something that will
	// work with our monitor driver.

	// Windows: "\\\\.\\DISPLAY3", etc. Can be used in EnumDisplaySettings/CreateDC.    
	const char* DisplayDeviceName;
	// MacOS
	int        DisplayId;
} old_ovrHmdDesc;

// Describes a vertex used for distortion; this is intended to be converted into
// the engine-specific format.
// Some fields may be unused based on ovrDistortionCaps selected. TexG and TexB, for example,
// are not used if chromatic correction is not requested.
typedef struct ovrDistortionVertex_
{
	ovrVector2f Pos;
	float       TimeWarpFactor;  // Lerp factor between time-warp matrices. Can be encoded in Pos.z.
	float       VignetteFactor;  // Vignette fade factor. Can be encoded in Pos.w.
	ovrVector2f TexR;
	ovrVector2f TexG;
	ovrVector2f TexB;
} ovrDistortionVertex;

// Describes a full set of distortion mesh data, filled in by ovrHmd_CreateDistortionMesh.
// Contents of this data structure, if not null, should be freed by ovrHmd_DestroyDistortionMesh.
typedef struct ovrDistortionMesh_
{
	ovrDistortionVertex* pVertexData;
	unsigned short*      pIndexData;
	unsigned int         VertexCount;
	unsigned int         IndexCount;
} ovrDistortionMesh;


//-----------------------------------------------------------------------------------
// ***** Platform-independent Rendering Configuration

// These types are used to hide platform-specific details when passing
// render device, OS and texture data to the APIs.
//
// The benefit of having these wrappers vs. platform-specific API functions is
// that they allow game glue code to be portable. A typical example is an
// engine that has multiple back ends, say GL and D3D. Portable code that calls
// these back ends may also use LibOVR. To do this, back ends can be modified
// to return portable types such as ovrTexture and ovrRenderAPIConfig.

typedef enum
{
	ovrRenderAPI_None,
	ovrRenderAPI_OpenGL,
	ovrRenderAPI_Android_GLES,  // May include extra native window pointers, etc.
	ovrRenderAPI_D3D9,
	ovrRenderAPI_D3D10,
	ovrRenderAPI_D3D11,
	ovrRenderAPI_Count
} ovrRenderAPIType;

// Platform-independent part of rendering API-configuration data.
// It is a part of ovrRenderAPIConfig, passed to ovrHmd_Configure.
typedef struct ovrRenderAPIConfigHeader_
{
	ovrRenderAPIType API;
	ovrSizei         RTSize;
	int              Multisample;
} ovrRenderAPIConfigHeader;

typedef struct ovrRenderAPIConfig_
{
	ovrRenderAPIConfigHeader Header;
	uintptr_t                PlatformData[8];
} ovrRenderAPIConfig;

// Used to configure slave D3D rendering (i.e. for devices created externally).
struct ovrD3D11ConfigData
{
	// General device settings.
	ovrRenderAPIConfigHeader Header;
	ID3D11Device*            pDevice;
	ID3D11DeviceContext*     pDeviceContext;
	ID3D11RenderTargetView*  pBackBufferRT;
	IDXGISwapChain*          pSwapChain;
};

union ovrD3D11Config
{
	ovrRenderAPIConfig Config;
	ovrD3D11ConfigData D3D11;
};


// Platform-independent part of eye texture descriptor.
// It is a part of ovrTexture, passed to ovrHmd_EndFrame.
//  - If RenderViewport is all zeros, will be used.
typedef struct ovrTextureHeader_
{
	ovrRenderAPIType API;
	ovrSizei         TextureSize;
	ovrRecti         RenderViewport;  // Pixel viewport in texture that holds eye image.
} ovrTextureHeader;

typedef struct ovrTexture_
{
	ovrTextureHeader Header;
	uintptr_t        PlatformData[8];
} ovrTexture;

// Used to pass D3D11 eye texture data to ovrHmd_EndFrame.
struct ovrD3D11TextureData
{
	// General device settings.
	ovrTextureHeader          Header;
	ID3D11Texture2D*          pTexture;
	ID3D11ShaderResourceView* pSRView;
};

union ovrD3D11Texture
{
	ovrTexture          Texture;
	ovrD3D11TextureData D3D11;
};



// For now.
// TBD: Decide if this becomes a part of HMDDesc
typedef struct ovrSensorDesc_
{
	// HID Vendor and ProductId of the device.
	short   VendorId;
	short   ProductId;
	// Sensor (and display) serial number.
	char    SerialNumber[24];
} ovrSensorDesc;


// Frame data reported by ovrHmd_BeginFrameTiming().
typedef struct ovrFrameTiming_
{
	// The amount of time that has passed since the previous frame returned
	// BeginFrameSeconds value, usable for movement scaling.
	// This will be clamped to no more than 0.1 seconds to prevent
	// excessive movement after pauses for loading or initialization.
	float			DeltaSeconds;

	// It is generally expected that the following hold:
	// ThisFrameSeconds < TimewarpPointSeconds < NextFrameSeconds < 
	// EyeScanoutSeconds[EyeOrder[0]] <= ScanoutMidpointSeconds <= EyeScanoutSeconds[EyeOrder[1]]

	// Absolute time value of when rendering of this frame began or is expected to
	// begin; generally equal to NextFrameSeconds of the previous frame. Can be used
	// for animation timing.
	double			ThisFrameSeconds;
	// Absolute point when IMU expects to be sampled for this frame.
	double			TimewarpPointSeconds;
	// Absolute time when frame Present + GPU Flush will finish, and the next frame starts.
	double			NextFrameSeconds;

	// Time when when half of the screen will be scanned out. Can be passes as a prediction
	// value to ovrHmd_GetSensorState() go get general orientation.
	double		    ScanoutMidpointSeconds;
	// Timing points when each eye will be scanned out to display. Used for rendering each eye. 
	double			EyeScanoutSeconds[2];

} ovrFrameTiming;


// Full pose (rigid body) configuration with first and second derivatives.
typedef struct old_ovrPoseStatef_
{
	ovrPosef     Pose;
	ovrVector3f  AngularVelocity;
	ovrVector3f  LinearVelocity;
	ovrVector3f  AngularAcceleration;
	ovrVector3f  LinearAcceleration;
	double       TimeInSeconds;         // Absolute time of this state sample.
} old_ovrPoseStatef;

// State of the sensor at a given absolute time.
typedef struct ovrSensorState_
{
	// Predicted pose configuration at requested absolute time.
	// One can determine the time difference between predicted and actual
	// readings by comparing ovrPoseState.TimeInSeconds.
	old_ovrPoseStatef  Predicted;
	// Actual recorded pose configuration based on the sensor sample at a 
	// moment closest to the requested time.
	old_ovrPoseStatef  Recorded;

	// Sensor temperature reading, in degrees Celsius, as sample time.
	float          Temperature;
	// Sensor status described by ovrStatusBits.
	unsigned int   StatusFlags;
} ovrSensorState;


// Rendering information for each eye, computed by either ovrHmd_ConfigureRendering().
// or ovrHmd_GetRenderDesc() based on the specified Fov.
// Note that the rendering viewport is not included here as it can be 
// specified separately and modified per frame though:
//    (a) calling ovrHmd_GetRenderScaleAndOffset with game-rendered api,
// or (b) passing different values in ovrTexture in case of SDK-rendered distortion.
typedef struct old_ovrEyeRenderDesc_
{
	ovrEyeType  Eye;
	ovrFovPort  Fov;
	ovrRecti	DistortedViewport; 	        // Distortion viewport 
	ovrVector2f PixelsPerTanAngleAtCenter;  // How many display pixels will fit in tan(angle) = 1.
	ovrVector3f ViewAdjust;  		        // Translation to be applied to view matrix.
} old_ovrEyeRenderDesc;


